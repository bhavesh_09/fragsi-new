# Generated by Django 3.0.5 on 2021-04-02 04:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('designs', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='user_feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(blank=True, max_length=250, null=True)),
                ('project_id', models.CharField(blank=True, max_length=250, null=True)),
                ('suggestion', models.CharField(blank=True, max_length=1000, null=True)),
                ('k_code', models.CharField(blank=True, max_length=250, null=True)),
                ('fragname', models.CharField(blank=True, max_length=250, null=True)),
                ('time', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='more_feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('similar_Kcode', models.CharField(blank=True, max_length=250, null=True)),
                ('similar_fragname', models.CharField(blank=True, max_length=250, null=True)),
                ('ai_Match_Percentage', models.CharField(blank=True, max_length=100, null=True)),
                ('stock', models.CharField(blank=True, max_length=100, null=True)),
                ('selling_price_inr', models.CharField(blank=True, max_length=100, null=True)),
                ('selling_price_usd', models.CharField(blank=True, max_length=100, null=True)),
                ('category', models.CharField(blank=True, max_length=100, null=True)),
                ('feedback_type', models.CharField(blank=True, max_length=100, null=True)),
                ('user_Option', models.CharField(blank=True, max_length=100, null=True)),
                ('user_Match_Perctange', models.CharField(blank=True, max_length=250, null=True)),
                ('user_Comment', models.CharField(blank=True, max_length=250, null=True)),
                ('user_Comment_Top', models.CharField(blank=True, max_length=250, null=True)),
                ('user_Comment_middle', models.CharField(blank=True, max_length=250, null=True)),
                ('user_Comment_base', models.CharField(blank=True, max_length=250, null=True)),
                ('feedback', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='feedback', to='designs.user_feedback')),
            ],
        ),
    ]
