from django.db import models

class xlimages(models.Model):
    images=models.CharField(max_length=1000)

    def __str__(self):
        return self.images
    

class user_feedback(models.Model):
    username = models.CharField(max_length=250, null=True, blank=True)
    project_id = models.CharField(max_length=250, null=True, blank=True)
    suggestion = models.CharField(max_length=1000, null=True, blank=True)
    k_code = models.CharField(max_length=250, null=True, blank=True)
    fragname = models.CharField(max_length=250, null=True, blank=True)
    time = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.k_code


class more_feedback(models.Model):
    feedback = models.ForeignKey(
        user_feedback, related_name='feedback', on_delete=models.CASCADE)
    similar_Kcode = models.CharField(max_length=250, null=True, blank=True)
    similar_fragname = models.CharField(max_length=250, null=True, blank=True)
    ai_Match_Percentage = models.CharField(
        max_length=100, null=True, blank=True)
    stock = models.CharField(max_length=100, null=True, blank=True)
    selling_price_inr = models.CharField(max_length=100, null=True, blank=True)
    selling_price_usd = models.CharField(max_length=100, null=True, blank=True)
    category = models.CharField(max_length=100, null=True, blank=True)
    feedback_type = models.CharField(max_length=100, null=True, blank=True)
    user_Option = models.CharField(max_length=100, null=True, blank=True)
    user_Match_Perctange = models.CharField(
        max_length=250, null=True, blank=True)
    user_Comment = models.CharField(max_length=250, null=True, blank=True)
    user_Comment_Top = models.CharField(max_length=250, null=True, blank=True)
    user_Comment_middle = models.CharField(
        max_length=250, null=True, blank=True)
    user_Comment_base = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.similar_Kcode

        
# Create your models here.
