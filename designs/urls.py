from django.urls import path
from django.contrib import admin
from designs.views import *

urlpatterns = [
    path('', index, name='index'),
    path('topN', topN, name='topN'),
    path('topTable', topTable,name='topTable'),
    path('compareKcode', compareKcode, name='compareKcode'),
    path('compareimage', compareimage, name='compareimage'),
    path('randomimage', randomimage, name='randomimage'),
    path('analyse', analyse, name='analyse'),
    path('error', error, name='error'),
    path('namesuggest',name_suggest,name ='namesuggest'),
    path("codesuggest",code_suggest,name ='codesuggest'),
    path("getImage",getImage,name ='getImage'),
    path("viewimage",viewimage,name ='viewimage'),
    path('admin', admin.site.urls, name ='admin'),
    path("showimage",showimage,name ='showimage'),
    path("check",check,name ='check'),
    path("extra",extra,name ='extra'),
    path('get_code',get_code,name="getcode"),
    path('get_name',get_name,name="getname"),
    
    
]
