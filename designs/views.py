from django.shortcuts import render
from django.http import JsonResponse,HttpResponse
from .models import xlimages,user_feedback,more_feedback
import pandas as pd
import numpy as np
import os 
import random
import string

from django.template.defaulttags import register
from django.conf import settings
STATIC_URL = settings.STATIC_ROOT
print(STATIC_URL)


@register.filter
def get_range(value):
    return range(0, value, 5)


@register.filter
def lrange(value):
    return range(0,value)


@register.filter
def to_int(value):
    return int(value)


    
def index(request):
    return render(request, "designs/topN.html") 


def topN(request):
    return render(request, "designs/topN.html")

def topTable(request):
    results = pd.read_pickle(os.path.join(STATIC_URL, 'designs', 'pickle', 'results_bhavesh.pkl'))
    full_name = results[0]
    diff_name = results[1]
    comb_name=  results[2]
    print(full_name)
    print(diff_name)
    context = {
        'full_name': full_name,
        'diff_name': diff_name,
        'comb_name': comb_name
    }
    return render(request, "designs/topNresult.html",context)



def compareKcode(request):
    comp1 = pd.read_csv(os.path.join(
        STATIC_URL, 'designs', 'csv', 'demo2.csv'))
    comparenames = comp1['comparenames'].values

    b = np.array([[random.choice(string.ascii_letters) +
                   'b' for i in range(51)] for i in range(1)], dtype='O')
    m = np.array([[random.choice(string.ascii_letters) +
                   '_random' for i in range(51)] for i in range(1)], dtype='O')
    n = np.random.randint(0, 255, [11, 51])
    p = np.concatenate((b, m, n)).tolist()
    row1 = p

    context = {
        'comparenames':comparenames,
        'row': row1,
        'columns': ['51']*51
    } 

    return render(request, "designs/compareKcode.html",context)


def compareimage(request):
    img_name = request.GET.get('image_name')
    img_name2 = request.GET.get('image_name2') 
    img_name3=request.GET.get('image_name3')
    option1 = request.GET.get('option')
    # path1 = os.path.join(excel_url, new_list[0]+'_Diffused.xlsx')
    # print(img_name,option)
    if (option1 == "Full-Full"):
        pass
    elif(option1 == "Diffused-Diffused"):
        img_name = img_name + '_diff'
        img_name2 = img_name2 + '_diff'
    else:
        img_name2 = img_name2 + '_diff'
    n1 = np.random.randint(0, 255, [11, 51])
    n2 = np.random.randint(0, 255, [11, 51])
    n3 = np.random.randint(0, 255, [11, 51])
    
    row1 = n1.tolist()
    row2 = n2.tolist()
    row3 = n3.tolist()
    comparearray = [row1, row2, row3]

    context = {
        'img_name': img_name,
        'img_name2': img_name2,
        'img_name3':img_name3,
        'z': comparearray,
        'columns': ['51']*51
    }
    return JsonResponse(context)



def randomimage(request):
    img_name3 = request.GET.get('image_name3')
    b = np.array([[random.choice(string.ascii_letters) +
                   'b' for i in range(51)] for i in range(1)], dtype='O')
    m = np.array([[random.choice(string.ascii_letters) +
                   '_random' for i in range(51)] for i in range(1)], dtype='O')
    n = np.random.randint(0, 255, [11, 51])
    p = np.concatenate((b, m, n)).tolist()
    n3 = np.random.randint(0, 255, [11, 51])
    row3 = n3.tolist()
    comparearray = [row3]
    context = {
        'img_name3': img_name3,
        'z': comparearray,
        'columns': ['51']*51
    }
    return JsonResponse(context)



def viewimage(request):
    test1 = pd.read_csv(os.path.join(STATIC_URL,'designs', 'csv', 'demo1.csv'))
    filename = test1['filename'].values
    db = request.GET.get('img')
    print(db)
    a = np.array([[random.choice(string.ascii_letters)+ 'b' for i in range(51)] for i in range(1)],dtype = 'O')
    x = np.array([[random.choice(string.ascii_letters)+ '_random' for i in range(51)] for i in range(1)],dtype = 'O')
    y = np.random.randint(0,255,[11,51])
    z = np.concatenate((a, x, y)).tolist()

    context = {
        'filename':filename,
        'img':db,
        'columns': ['51']*51,
        'row3': z
         }
    return render(request, "designs/view.html",context)

def showimage(request):
    img_name = request.GET.get('image_name')
    # print(img_name)
    option = request.GET.get('option')
    # print(img_name,option)
    if (option == "Full"):
        url = img_name + '.png'
    else:
        url = img_name + '_diff.png'
        img_name = img_name + '_diff'
    rr = np.random.randint(0, 255, [11, 51]) 
    y = np.random.randint(0, 255, [11, 51])
    
    z = y.tolist()
    return JsonResponse({'extra': img_name, 'url' : url , 'row4' : z})





def analyse(request):
    return render(request, "designs/analyse.html")


def error(request):
    return render(request, "designs/error.html")


def check(request):
    return render(request, "designs/view2.html")









df = pd.DataFrame()
k_code_list = []
fragname_list = []
k_code_list_non = []
fragname_list_non = []
#useful for making the search
data = dict()#k code is a key and fragrance name is a value
data2 = dict()#fragrance name is a key and k code is a value

def initialize_data():
    global df
    global k_code_list
    global fragname_list
    global k_code_list_non
    global fragname_list_non
    global data
    global data2

    df = pd.read_pickle(os.path.join(STATIC_URL, 'designs', 'pickle', 'final_mapping.pkl'))
    # result = df['result'].values
    print(df)
    #converting into the list
    k_code_list = df['Kcode'].tolist()
    fragname_list = df['Fragname'].tolist()

    df1 = df[~df['Kcode'].str.contains('AARC-')]
    k_code_list_non = df1['Kcode'].tolist()
    fragname_list_non = df1['Fragname'].tolist()

    #creating the dict data for searching using code
    for each_k_code in k_code_list:
        if each_k_code not in data:
            data[each_k_code] = fragname_list[k_code_list.index(each_k_code)]
    #creating the dict data2 for searching using fragrance_name
    for each_name in fragname_list:
        if each_name not in data2:
            data2[each_name] = k_code_list[fragname_list.index(each_name)]


#for finding the matching result for the 
def get_matched_elements(strings,element=None,page=1):
    
    matched_found = []
    result = dict()
    
    #if user has not searched anything then serving based on the scrolling action
    if element is None:
        
        start = (page - 1) * 10
        end = 10 * page

        for each_element in strings[start:end]:
            matched_found.append({'id':each_element.replace("#","hashtag"),'text':each_element})
        
        return {"results":matched_found ,"count_filtered":len(strings) - page*10,"datasent":len(matched_found) }
        

    
    #if user is searching something giving the suggested result
    if element is not None:   
        for string in strings:
            # if user searched query is their in any record append that record
            if string.lower() in element.lower() or element.lower() in string.lower(): 
                matched_found.append({'id':string.replace("#","hashtag"),'text':string})

        return {"results":matched_found ,"count_filtered":0,"datasent":len(matched_found) }
            



#when user will click on any fragrance name then its corresponding code will be return as response
def get_code(request):
    if request.method == "GET":
        name =request.GET['n'].strip().replace("hashtag",'#')
        if name is not None:    
            res = f"""<option value='{data2[name].replace("#","hashtag")}'> {data2[name]} </option>"""
            return HttpResponse(res)


#when user will click on any kcode name then its corresponding fragrance name will be return as response
def get_name(request):
    if request.method == "GET":
        try:
            code = request.GET['c'].strip().replace("hashtag",'#')
            if code is not None:
                res = f"""<option value='{data[code].replace("#","hashtag")}'> {data[code]} </option>"""
                print(f"Sending response for code={code} and{res}")
                return HttpResponse(res)
        except:
            pass



    



def code_suggest(request):
    url = request.get_full_path()

    if request.is_ajax() or request.method == "GET":
        try:
            element = request.GET.get('term')
        except:
            pass
        try:
            page = int(request.GET.get("page"))
        except:
            page = 1
    
    if 'nonAARC' in url:
        content = get_matched_elements(k_code_list_non,element,page)
    else:
        content = get_matched_elements(k_code_list,element,page)

    return JsonResponse(content,safe=False)

def name_suggest(request):  
    url = request.get_full_path()

    if request.is_ajax() or request.method == "GET":
        try:
            element = request.GET.get('term')
            page = int(request.GET.get('page'))
        except:
            page = 1
        
    if 'nonAARC' in url:
        content = get_matched_elements(fragname_list_non,element,page)
    else:   
        content = get_matched_elements(fragname_list,element,page)

    return JsonResponse(content,safe=False) 

def getImage(request):
    kcode = request.POST.get("kcode", "").replace("hashtag","#")
    print(kcode)

    final_mapping_df = pickle.load(open(os.path.join(STATIC_URL, 'designs', 'pickle', 'final_mapping.pkl'), "rb"))
    filename = final_mapping_df[final_mapping_df['Kcode'] == kcode]['Filename'].values[0]
    print(filename)
    a_filenames = os.listdir(a_drive) #list of filename in folder "A drive"
    g_filenames = os.listdir(g_drive)
    g_filenames_olfactory = os.listdir(g_drive_olfactory) #list of filename in folder "G drive olfactory"

    if filename+'.xlsx' in a_filenames:
        path = a_drive
    elif filename+'.xlsx' in g_filenames:
        path = g_drive
    elif filename+'.xlsx' in g_filenames_olfactory:
        path = g_drive_olfactory
    
    shutil.copy(os.path.join(path, filename+'.xlsx'), excel_file_path)
    
    df4 = pd.read_excel(os.path.join(excel_file_path, filename+'.xlsx'))
    df4 = df4.fillna('')
    df4 = df4.values
    df4 = df4.tolist()
    context = {
        'kcode':kcode,
        'df': df4,
    }
    return JsonResponse(context)



def extra(request):
    a = range(20)
    return render(request,'designs/extra.html',{'a':a})

# Create your views here.
initialize_data()